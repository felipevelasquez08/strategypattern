import Foundation

public protocol FilterStrategy {
    func filter(_ key: Int, _ numbers: [Int]) -> Int
}
