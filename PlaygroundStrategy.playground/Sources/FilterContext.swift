import Foundation

public class FilterContext {
    
    private var filterStrategy: FilterStrategy
    
    public init(_ filterStrategy: FilterStrategy) {
        self.filterStrategy = filterStrategy
    }
    
    public func filter(_ key: Int, _ numbers: [Int]) -> Int {
        return filterStrategy.filter(key, numbers)
    }
}
