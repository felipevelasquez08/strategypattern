import Foundation

public enum FilterType {
    case secuential
    case binary
}
