import Foundation

public class BinaryStrategy: FilterStrategy {
    
    public init(){}
    
    public func filter(_ key: Int, _ numbers: [Int]) -> Int {
        var start = 0
        var half = -1
        var end = numbers.count - 1
        while start <= end {
            half = (start + end) / 2
            if numbers[half] == key {
                return half
            } else if numbers[half] < key {
                start = half + 1
            } else {
                end = half - 1
            }
        }
        return -1
    }
    
}
