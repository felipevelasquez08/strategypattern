import Foundation

public class SecuentialStrategy: FilterStrategy {
    
    public init() {}
    
    public func filter(_ key: Int, _ numbers: [Int]) -> Int {
        var index = -1
        for i in 0..<numbers.count {
            if key == numbers[i] {
                index = i
                break
            }
        }
        return index
    }
    
    
}
