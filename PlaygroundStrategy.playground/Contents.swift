let filterType = FilterType.binary
var filterStrategy: FilterStrategy
switch filterType {
case FilterType.secuential:
    filterStrategy = SecuentialStrategy()
case FilterType.binary:
    filterStrategy = BinaryStrategy()
}
let filterContext = FilterContext(filterStrategy)
let array = [1, 2, 4, 6, 8, 9, 10, 21]
let key = 2
let result = filterContext.filter(key, array)
print("Index is: \(result)")
